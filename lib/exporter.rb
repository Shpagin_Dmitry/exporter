module Exporter
  require 'rubyXL'
  class XLSX
    def self.model_attributes(name)
      Object.const_get(name).attribute_names
    end
    def self.get_workbook(path)
      @workbook = RubyXL::Parser.parse(path)
    end
    def self.generate_workbook
      @workbook = RubyXL::Workbook.new
      @workbook.worksheets.slice!(0)
    end
    def self.get_export_data(model)
      Object.const_get(model).all
    end
    def self.export_models(array_of_strings)
      self.generate_workbook
      array_of_strings.each{|el|
        worksheet = @workbook.add_worksheet
        worksheet.sheet_name=el
        self.model_attributes(el).each.with_index {|atr,i|
          worksheet.insert_cell(0, i, atr, formula = nil, :right)
        }
        self.get_export_data(el).each.with_index {|row, i|
          row.as_json.to_a.map{|el| el = el[1]}.each.with_index {|col, j|
            worksheet.insert_cell(i+1, j, col, formula = nil, :right)
          }
        }
      }
      @workbook.write("file.xlsx")
    end
    def self.import_by_workbook(path)
      self.get_workbook(path)
      errors = 0;
      @workbook.worksheets.each { |ws|
          model = Object.const_get(ws.sheet_name)
          model.delete_all
          headers = []
          ws.each { |row| 
            if row.r == 1 then 
              headers = [].tap{|m| row && row.cells.each { |cell|
                      m.push(cell && cell.value)
                    }
                  }
            else
              record = model.send('new')
              column = 0
              row.cells.each{ |cell|
                record.send("#{headers[column]}=",cell && cell.value)
                column+=1
              }
              begin
                record.save!
              rescue
                errors+=1;
              end
            end
          } 
      }
      "finish, errors: #{errors}"
    end
  end
end
