# README #

### What is this repository for? ###
This gem is created for simple export models from db, and importing values for existing models by .xlsx files

### How do I get set up? ###
add in gemfile: gem 'exporter', :path=> local/path/to/gemfile

### Contribution guidelines ###
##USAGE:

* Exporting
Exporter::XLSX.export_models(['Exaple_model1','User','Order'])
-> file.xlsx
* Importing
Exporter::XLSX.import_by_workbook('Usr/local/downloads/file.xlsx')
-Will delete all data from existing in workbook models and import new one