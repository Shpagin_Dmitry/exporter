Gem::Specification.new do |s|
  s.name        = 'exporter'
  s.version     = '0.1.14'
  s.date        = '2016-11-07'
  s.summary     = "Hola!"
  s.description = "Gem for RoR importing data by excel files"
  s.authors     = ["Dmitry Shpagin"]
  s.email       = 'sofakingworld@gmail.com'
  s.files       = ["lib/exporter.rb"]
  s.homepage    =
    'http://rubygems.org/gems/hola'
  s.license       = 'MIT'
  s.add_development_dependency(%q<rubyXL>, [">= 3.3.21"])
  s.add_dependency(%q<rubyXL>, [">= 3.3.21"])
end